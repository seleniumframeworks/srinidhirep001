package com.experitest.auto;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import java.net.URL;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class IOSDemoTest extends BaseTest {
	protected IOSDriver<IOSElement> driver = null;
	private final String strUsername="company";
	private final String strPassword="company";
	@BeforeMethod
	@Parameters("deviceQuery")
	public void setUp(@Optional("@os='ios'") String deviceQuery) throws Exception {
		init(deviceQuery);
		String accessKey = "eyJ4cC51Ijo4MjQwODIyLCJ4cC5wIjo4MjQwODIxLCJ4cC5tIjoiTVRVNE9EazBNVE16TURBMU1RIiwiYWxnIjoiSFMyNTYifQ.eyJleHAiOjE5MDQzMDE0NjUsImlzcyI6ImNvbS5leHBlcml0ZXN0In0.JF3pC8JkbtDSAO3J47Sx3ofHrJCcMKMwGydvyUBx4dA";
		
		dc.setCapability("accessKey", accessKey);
		dc.setCapability(MobileCapabilityType.APP, "cloud:com.experitest.ExperiBank");
		dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.experitest.ExperiBank");
		dc.setCapability("instrumentApp", true);
		dc.setCapability("testName", "IOSDemoTest");
		driver = new IOSDriver<>(new URL(getProperty("url",cloudProperties)+"/wd/hub/"), dc);
	}

	@Test
	public void test() {
		
		driver.findElement(in.Repo.obj("LoginPage.Username")).sendKeys(strUsername);
		driver.findElement(in.Repo.obj("LoginPage.Password")).sendKeys(strPassword);
		driver.findElement(in.Repo.obj("LoginPage.Login")).click();
		
		String strBalanceMessage=driver.findElement(in.Repo.obj("BalancePage.YourBalance")).getText();
		if(strBalanceMessage.trim().startsWith("Your balance is"))
			Reporter.log(strBalanceMessage, true);
		else
			Reporter.log(strBalanceMessage,false);
		
	}

	
	@AfterMethod
	public void tearDown() {
		driver.closeApp();
		driver.close();
		driver.quit();
	}

}
